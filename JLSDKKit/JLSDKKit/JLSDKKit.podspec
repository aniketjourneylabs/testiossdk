Pod::Spec.new do |s|

  s.name         = "JLSDKKit"
  s.version      = "0.0.6"
  s.summary      = "A short summary of JLSDKKit."
  s.description  = "JLSDKKit for Ticketing module"
  s.homepage     = "https://dev.journeylabs.io"
  s.license      = { :type => 'MIT'}
  s.source       = { :git => "https://aniketjourneylabs@bitbucket.org/aniketjourneylabs/testiossdk.git" , :tag => s.version.to_s}
  s.authors      = "JourneyLabs"
  s.ios.deployment_target = '10.0's
  s.ios.vendored_frameworks = 'JLSDKKit/**/JLSDKKit.framework'


end
