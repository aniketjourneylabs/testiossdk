Pod::Spec.new do |s|

  s.name         = "JLSDKKit"
  s.version      = "0.0.5"
  s.summary      = "A short summary of JLSDKKit."
  s.description  = "JLSDKKit for Ticketing module"
  s.homepage     = "https://dev.journeylabs.io"
  s.license      = { :type => 'MIT'}
  s.source       = { :git => "https://aniketjourneylabs@bitbucket.org/aniketjourneylabs/testiossdk.git" , :tag => "0.0.4"}
  s.authors      = "JourneyLabs"
  s.ios.deployment_target = '10.0'
  s.ios.vendored_frameworks = 'JLSDKKit/Pods/JLSDKKit.framework'


end
